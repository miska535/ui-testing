const expect = require('chai').expect;
const {Builder, By, Key, WebElement, Browser} = require('selenium-webdriver');

// const BASE_URL = "https://tryphp.w3schools.com/showphp.php?filename=demo_form_validation_complete";
const BASE_URL = "https://tryphp.w3schools.com/demo/demo_form_validation_complete.php";
const sleep = (seconds) => new Promise((resolve)=> setTimeout(resolve, seconds * 1000));

const NAME = "Testaaja";
const EMAIL = "testaaja@testaajat.fi"
const WEBSITE = "www.nonexisting.com";
const COMMENT = "Hello world!";
const GENDER = "male";

describe('UI TESTS', () => {
    /** @type {import('selenium-webdriver').ThenableWebDriver} */
    let driver = null;
    before(async () => {
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .build();
        await driver.get(BASE_URL);
        // await sleep(1);
    });
    // it ("Can accept", async() => {
    //     const acceptcookies_button = await driver.findElement(By.id("accept-choices"));
    //     await acceptcookies_button.click();
    //     await sleep(1);
    // });
    it("Set name input", async () => {
        const name_input = await driver.findElement(By.name("name"));
        await name_input.sendKeys(NAME);
        const name = await name_input.getAttribute("value");
        expect(name).to.eq(NAME);
    });
    it("Set email input", async () => {
        const email_input = await driver.findElement(By.name("email"));
        await email_input.sendKeys(EMAIL);
        const email = await email_input.getAttribute("value");
        expect(email).to.eq(EMAIL);
        await sleep(0.2);
    });
    it("Set website input", async () => {
        const website_input = await driver.findElement(By.name("website"));
        await website_input.sendKeys(WEBSITE);
        const website = await website_input.getAttribute("value");
        expect(website).to.eq(WEBSITE);
        await sleep(0.2);
    });
    it("Set Comment input", async () => {
        const comment_textarea = await driver.findElement(By.name("comment"));
        await comment_textarea.sendKeys(COMMENT);
        const comment = await comment_textarea.getAttribute("value");
        expect(comment).to.eq(COMMENT);
        await sleep(0.2);
    });
    it("Set Gender input", async () => {
        const gender_input = await driver.findElement(By.css(`input[value=${GENDER}]`));

        let enabled = await gender_input.isSelected();
        expect(enabled).to.be.false;
        await gender_input.click();
        enabled = await gender_input.isSelected();
        expect(enabled).to.be.true;
        await sleep(0.2);

    });
    it("Submit form", async() => {
        const name_input = await driver.findElement(By.name("name"));
        let succeed = false;
        try {
            await name_input.submit();
            succeed = true;
        }catch(error) {};
        expect(succeed).to.be.true;
        await sleep(1);
    })
    after(async () => {
        await driver.close();
    });
});